import { 
    StyleSheet, 
    Text, 
    View, 
    SafeAreaView, 
    Image, 
    ImageBackground, 
    TouchableOpacity, 
    FlatList, 
    Dimensions 
  } from 'react-native'
  import React from 'react'
// import COLORS from '../../consts/colors';
  import products from '../constants';
  import Icon from 'react-native-vector-icons/MaterialIcons';
  
  const width = Dimensions.get('window').width / 2 - 30;
  
  export default function Home({navigation}) {
  
    const CategoryList = () => {
      return (
        <View style={styles.categoryContainer}>      
          <Text style={{fontSize: 18, fontWeight: 'bold', color: '#153853'}}>Take surveys and win points </Text>
          <Text style={{fontSize: 17, color: '#153853', textDecorationLine: 'underline'}}>View All</Text>
        </View>
      )
    }
  
     const Card = ({plant}) => {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => navigation.navigate('Details', plant)}>
          <View style={styles.card}>
            <View style={{alignItems: 'flex-end'}}>
              <View style={{ paddingBottom: 25 }}>
              <View
                style={{
                  width: 60,
                  height: 30,
                  position: 'absolute',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#DAEFFF',
                  right: -12,
                  top: -12                 
                }}>           
  
                
               <Image 
                source={require('../assets/images/Time.png')}
                resizeMode="contain"
                style={{
                  position: 'absolute',
                  height: 15,
                  width: 15,
                  top: 10,
                  left: 8
              }}
            />    
            
              <Text
                style={{                
                  marginLeft: 10,
                  color: '#153853',
                  fontSize: 10,
                  fontWeight: 'bold',
                  top: 2,
                  left: 10,
                  
                }}
              >
                3 Min
              </Text>           
              
                
              </View>
              </View>
            </View>
  
            <View
              style={{
                height: 100,
                alignItems: 'center',
              }}>
              <Image
                source={plant.img}
                style={{flex: 1, resizeMode: 'contain'}}
              />
            </View>
  
            {/* <Text style={{fontWeight: 'bold', fontSize: 17, marginTop: 10}}>
              {plant.name}
            </Text> */}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: '#153853'}}>
                {plant.price}
              </Text>
              <View
                style={{
                  height: 25,
                  width: 30,
                  // backgroundColor: COLORS.green,
                  // borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{fontSize: 13, color: '#153853', fontWeight: 'bold'}}>
                  2/10
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    };
  
    function renderCard() {
      return (
        <ImageBackground
        source={require('../assets/images/home.png')}
        style={{
          height: 200,
          width: "100%",
          marginTop: 10,
          borderRadius: 12,
          overflow: 'hidden'
        }}
        >
          <View
            style={{
              position: 'absolute',
              top: 30,
              left: 10,
              right: 0,
              paddingHorizontal: 24
            }}
          >
            <Text 
              style={{
                color: 'white',
                fontSize: 16,
                lineHeight: 22,
                left: 10
              }}
            >
              Your total points
            </Text>
  
           
            <Image 
              source={require('../assets/images/coin.png')}
              resizeMode="contain"
              style={{
                position: 'absolute',
                height: 50,
                width: 90,
                top: 30,
                left: 10
              }}
            />    
            
              <Text
                style={{                
                  marginLeft: 60,
                  color: '#F7B046',
                  fontSize: 33,
                  fontWeight: 'bold',
                  top: 9,
                  left: 10,
                  
                }}
              >
                5000000000
              </Text>
  
              <TouchableOpacity
                style={{
                  top: 30,
                  left: 9,
                  backgroundColor: 'white',
                  borderRadius: 7,
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 40,
                  width: 115,
                }}
              >
                <Text style={{fontWeight: 'bold', fontSize: 13, color: '#153853'}}>Redeem Points</Text>
              </TouchableOpacity>
          </View>
        </ImageBackground>
      )
    }
  
  
    return (    
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <View style={styles.header}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontSize: 20}}>Hi, </Text>
              <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 8, color: '#153853'}}>Saleem</Text>
              </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <Image 
            source={require('../assets/images/notification.png')} 
            style= {{
              height: 25,
              width: 25,            
            }}
          />
          <View 
            style={{
              width: 10,
              height: 10,
              borderRadius: 10,
              backgroundColor: '#F7B046',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'relative',
              top: -4,
              right: 6
            }}
          />
          <View style={{width: 15}} />
          <Image 
            source={require('../assets/images/profile.jpg')} 
            style= {{
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          />
          </View>
        </View>      
        <View
            style={{
              flex: 1,
              backgroundColor: 'white'
            }}
          >
            {/* Body */}
            {renderCard()}
            <View style={{ width: "85%", left: 30  }}>
              <CategoryList />
            </View>
            <FlatList
              columnWrapperStyle={{justifyContent: 'space-between'}}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                marginTop: 10,
                paddingBottom: 50,
              }}
              numColumns={2}
              data={plants}
              renderItem={({item}) => {
                return <Card plant={item} />;
              }}
            />
        </View>
        
        
  
      </SafeAreaView>
      
    )
  }
  
  const styles = StyleSheet.create({
  
      categoryContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        justifyContent: 'space-between'
        
      },
      categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
      categoryTextSelected: {
        color: 'green',
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: 'green',
      },
  
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      image: {
        height: 200,
        margin: 2
      },
      btn: {
        height: 70,
        width: 150,
        backgroundColor: '#46F7AD',
        marginTop: 170,
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 30
    },
    header: {
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 30
    },
    card: {
      height: 190,
      backgroundColor: '#EFEFEF',
      width,
      marginHorizontal: 15,
      borderRadius: 15,
      marginBottom: 20,
      padding: 15,
    },
  
  })