import { 
    StyleSheet, 
    Text, 
    View, 
    SafeAreaView, 
    Image, 
    Dimensions,
    TouchableOpacity
  } from 'react-native'
import React from 'react'
import { COLORS, FONTS, SIZES, icons, images } from "../../constants";
import {
    IconTextButton
  } from '../../components';

  
  const width = Dimensions.get('window').width / 2 - 30;
  
  export default function Redeem ({navigation}) {
    return (    
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <View style={styles.header}>
          <View>
            <View style={{flexDirection: 'row'}}>
              
              <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 5, color: '#153853'}}>Redeem Points</Text>
              </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <Image 
            source={require('../../assets/images/notification.png')} 
            style= {{
              height: 25,
              width: 25,            
            }}
          />
          <View 
            style={{
              width: 10,
              height: 10,
              borderRadius: 10,
              backgroundColor: '#F7B046',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'relative',
              top: -4,
              right: 6
            }}
          />
          <View style={{width: 15}} />
          <Image 
            source={require('../../assets/images/profile.jpg')} 
            style= {{
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          />
          </View>
        </View>    

       <View
        style={{
            flexDirection: 'row',
            marginTop: 30,
            paddingHorizontal: SIZES.radius
        }}
       >

        <IconTextButton
                label="Redeem History"
                containerStyle={{
                    flex: 1,
                    height: 40,
                    marginRight: SIZES.radius,
                    color: COLORS.white,
                    
            }}
            onPress={() => console.log("Redeem")}
            />

        <IconTextButton
                label="Redeem"
                containerStyle={{
                    flex: 1,
                    height: 40,
                    color: COLORS.white
            }}
            onPress={() => console.log("Redeem")}
            />

       

       </View>
        
        


       </SafeAreaView>

      

      
    )
  }
  
  const styles = StyleSheet.create({
  
      categoryContainer: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        justifyContent: 'space-between'
        
      },
      categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
      categoryTextSelected: {
        color: 'green',
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: 'green',
      },
  
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      image: {
        height: 200,
        margin: 2
      },
      btn: {
        height: 70,
        width: 150,
        backgroundColor: '#46F7AD',
        marginTop: 170,
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 30
    },
    header: {
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 30
    },
    card: {
      height: 190,
      backgroundColor: '#EFEFEF',
      width,
      marginHorizontal: 15,
      borderRadius: 15,
      marginBottom: 20,
      padding: 15,
    },
  
  })