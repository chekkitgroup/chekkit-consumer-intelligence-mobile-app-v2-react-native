import React, { useState } from "react";
import { View, Text, SafeAreaView, TouchableOpacity, FlatList, Image, StyleSheet, Platform } from "react-native";
import { COLORS, FONTS, SIZES, icons, images } from "../../constants";
import { Header, IconButton } from '../../components'


// const CATEGORIES = [
//     {
//       id: '1',
//       title: 'All',
//     },
//     {
//       id: '2',
//       title: 'Bodywash',
//     },
//     {
//       id: '3',
//       title: 'Face care',
//     },
//     {
//       id: '4',
//       title: 'Cosmetics',
//     },
//     {
//       id: '5',
//       title: 'Creams',
//     },
//   ];
  
  const PRODUCTS = [
    {
      id: '1',
      name: 'Food and Agro',
      image: require('../../assets/images/fmn.png'),
    },
    {
      id: '2',
      name: 'Indomie',
      image: require('../../assets/images/indomie.png'),
    },
    {
      id: '3',
      name: 'Africa CDC',
      image: require('../../assets/images/africacdc.png'),
    },
    {
      id: '4',
      name: 'Nivea',
      image: require('../../assets/images/nivea.png'),
    },
    {
      id: '4',
      name: 'Merk',
      image: require('../../assets/images/merck.png'),
    },
    {
        id: '5',
        name: 'Nabros',
        image: require('../../assets/images/nabros.png'),
    },
  ];

  
  

const BrandSurvey = ({ navigation }) => {
    
    const renderHeader = () => {
        return (
            <Header 
               title="Brand Survey"
               containerStyle={{
                   height: 20,
                   marginHorizontal: SIZES.padding,
                   marginTop: 40
               }}
               leftComponent={
                   <IconButton 
                       icon={icons.back}
                       containerStyle={{
                           width: 40,
                           height: 22,
                           justifyContent: 'center',
                           alignItems: 'center',
                           // borderWidth: 1,
                           borderRadius: SIZES.radius,
                           borderColor: COLORS.gray2
                       }}
                       iconStyle={{
                           width: 20,
                           height: 20,
                           tintColor: COLORS.primary
                       }}
                       onPress={() => navigation.navigate('Dashboard')}
                   />
               }
            />
        )
     }
    
    return(
        <SafeAreaView>
            <View
                style={{
                    top: -20
                }}
            >
                {renderHeader()}
            </View>
           
            <View style={styles.productContainer}>
                <FlatList
                    data={PRODUCTS}
                    numColumns={2}
                    showsVerticalScrollIndicator={false}
                    key={({item}) => item.id}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={() => {
                          navigation.navigate('details', {
                            id: item.id,
                            name: item.name,
                            image: item.image,
                          })
                        }}>
                            <View style={styles.product}>
                                
                                <Image source={item.image} style={styles.image} />
                                <Text style={styles.subtitle}>{item.name}</Text>
                                
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </SafeAreaView>
    )
}

export default BrandSurvey;

const styles = StyleSheet.create({
    header: {
        paddingHorizontal: 15,
        paddingTop: 5,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    titleBold: {
        color: COLORS.primary,
        fontSize: SIZES.h2,
        fontWeight: "bold"
    },
    title: {
        color: COLORS.primary,
        fontSize: SIZES.h2,
        marginTop: -2,
    },
    categoriesTab: {
        paddingTop: 10,
        paddingHorizontal: 15,
        marginLeft: 10,
    },
    category: {
        borderRadius: 20,
        minWidth: 80,
        paddingVertical: 10,
        paddingHorizontal: 15,
        alignItems: 'center',
    },
    subtitle: {
        color: COLORS.accent,
        fontWeight: "bold",
    },
    productContainer: {
        paddingHorizontal: 15,
        paddingVertical: 5,
        marginBottom: Platform.OS === "ios" ? 100 : 150,
    },
    product: {
        margin: 5,
        padding: 10,
        backgroundColor: COLORS.white2,
        borderRadius: 15,
        width: SIZES.width / 2  - 25,
        alignItems: 'center',
    },
    image: {
        width: SIZES.width / 3,
        height: 150,
        resizeMode: 'contain',
    },
    price: {
        fontWeight: "bold"
    },
    add: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 1,
        backgroundColor: COLORS.white2,
        padding: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 25,
        shadowColor: COLORS.primary,
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 2,
        shadowOpacity: 0.3,
        elevation: 10,
    }

});