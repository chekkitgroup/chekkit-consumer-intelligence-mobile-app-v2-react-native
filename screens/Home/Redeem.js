import { 
  StyleSheet, 
  Text, 
  View, 
  SafeAreaView, 
  Image, 
  ImageBackground, 
  TouchableOpacity, 
  FlatList, 
  Dimensions,
  TextInput
} from 'react-native'
import React from 'react'
import products from '../../consts/products';
import Icon from 'react-native-vector-icons/MaterialIcons';


const width = Dimensions.get('window').width / 2 - 30;

import { FONTS, SIZES, COLORS, icons, images } from '../../constants';
import {GroupButton, FormInput} from '../../components';

export default function Redeem ({navigation}) {


  const buttons = [
    {
      id: 1,  
      activated: true,
        text: 'Redeem History',
        onPress: () => {},
    },
    {
      id: 2,
      activated: false,
      text: 'Redeem',
      onPress: () => {navigation.push('RedeemPrize')}
    },
]




  return (    
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View style={styles.header}>
        <View>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 20}}>Hi, </Text>
            <Text style={{fontSize: 20, fontWeight: 'bold', marginLeft: 8, color: '#153853'}}>Saleem</Text>
            </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        <Image 
          source={require('../../assets/images/notification.png')} 
          style= {{
            height: 25,
            width: 25,            
          }}
        />
        <View 
          style={{
            width: 10,
            height: 10,
            borderRadius: 10,
            backgroundColor: 'gold',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'relative',
            top: -4,
            right: 6
          }}
        />
        <View style={{width: 15}} />
        <Image 
          source={require('../../assets/images/profile.jpg')} 
          style= {{
            height: 50,
            width: 50,
            borderRadius: 25
          }}
        />
        </View>
      </View>  

       <View style={styles.container}>
         <GroupButton
          activeColor='#153853'
          buttons={buttons}
         >

         </GroupButton>

         <View
          style={{
            marginTop: 40,
            flexDirection: 'row',
            paddingHorizontal: 20,
          }}>
           <View
            style={styles.inputContainer}
           >
             <Icon name="search" size={28} />
             <TextInput
            style={{flex: 1, fontSize: 18}}
            placeholder="Type here to search"
          />
           </View>
           <View style={styles.sortBtn}>
          <Icon name="tune" size={28} color={COLORS.white} />
        </View>

         

         </View>

         <View
          style={styles.cardContainer}
         >
           <View style={{flexDirection: 'row'}}>
            
             <View style={{justifyContent: 'center', alignItems: 'center', marginLeft: 50, }}>
                <Text style={{color: COLORS.primary, fontSize: SIZES.h3, top: -10}}>Your Total Point</Text>

                <Text style={{color: '#F7B046', fontSize: 33, fontWeight: 'bold'}}>5000</Text>

             </View>
             <View style={{marginLeft: 50}}>
                <Image 
                source={require('../../assets/images/bgstars.png')} 
                resizeMode="contain" 
                style = {{ width: 130, height: 150 }}
                />
             </View>
           </View>
         </View>


        




         

       </View>
      
      
      

    </SafeAreaView>

    

    
  )
}

const styles = StyleSheet.create({

    categoryContainer: {
      flexDirection: 'row',
      marginTop: 10,
      marginBottom: 20,
      justifyContent: 'space-between'
      
    },
    categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
    categoryTextSelected: {
      color: 'green',
      paddingBottom: 5,
      borderBottomWidth: 2,
      borderColor: 'green',
    },

    container: {
      flex: 1,
     
    },
    image: {
      height: 200,
      margin: 2
    },
    inputContainer: {
      flex: 1,
      height: 50,
      borderRadius: 10,
      flexDirection: 'row',
      backgroundColor: COLORS.lightGray2,
      alignItems: 'center',
      paddingHorizontal: 20,
    },
    
  header: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30
  },
  sortBtn: {
    width: 50,
    height: 50,
    marginLeft: 10,
    backgroundColor: COLORS.primary,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardContainer: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    backgroundColor: COLORS.lightGray2
  }
})