import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, StatusBar, ImageBackground } from 'react-native';

import { FONTS, SIZES, COLORS, icons, images } from '../../constants';
import {
  TextButton,
  Header,
  IconButton
} from '../../components';

import { SafeAreaView } from 'react-native-safe-area-context';

const DetailsScreen = ({ navigation, route }) => {

  const product = route.params;

  const renderHeader = () => {
    return (
        <SafeAreaView 
          style={{ 
            backgroundColor: '#C4C4C4',
            height: 340,
            width: "100%"
          }}
        >
        <Header 
               containerStyle={{
               height: 20,
               marginHorizontal: SIZES.padding,
               marginTop: 10,
               marginLeft: 10,
               backgroundColor: '#C4C4C4',
               
           }}
           leftComponent={
               <IconButton 
                   icon={icons.back}
                   containerStyle={{
                       width: 40,
                       height: 22,
                       justifyContent: 'center',
                       alignItems: 'center',
                       // borderWidth: 1,
                    //    borderRadius: SIZES.radius,
                    //    borderColor: COLORS.white
                   }}
                   iconStyle={{
                       width: 20,
                       height: 20,
                       tintColor: COLORS.primary
                   }}
                   onPress={() => navigation.navigate('MainApp')}
               />
           }
        />

        <ImageBackground
            resizeMode="contain"
            source={product.img}
            style={{
                height: 150,
                top: 15,
            }}>
        </ImageBackground>  
        </SafeAreaView>
    )
 }
 
 return (
        <>
           {renderHeader()}

        {/* Footer */}
        <View style={{marginTop: 50,  flex: 1,}}>
        
        <View
            style={{
                left: 20,
                fontSize: 40,
            }}
        >
           
            <Text style={{color: COLORS.primary, ...FONTS.body2,}}>Indomie</Text>
            <Text style={{color: COLORS.primary, ...FONTS.body4,}}> 
              Earn 50 points        
            </Text>
            </View>
        
        <View>
          {/* Render user image , name and date */}
          
          <Text
            style={{
                color: COLORS.primary, 
                ...FONTS.h3,
                top: 15,
                left: 20,
            }}
          >Survey Description</Text>
          <Text style={style.comment}>
          You get reward instantly just for answering some questions which wouldnt’t 
          even take much of your time . What are you waiting for?, hurry and click 
          the button below and start winning amazing prizes!         
          </Text>
          <View>
          <TextButton
          label="Take Survey (+50)"          
          buttonContainerStyle={{
            
           height: 60,
           marginTop: 40,
           left: 20,
           marginRight: 40,
           borderRadius: 12
              
          }}
          onPress={() => navigation.navigate('Survey')}
        />
          </View>
        </View>
        
 
      </View>
      </>

  );
};

const style = StyleSheet.create({
  detailsContainer: {
    height: 120,
    backgroundColor: COLORS.white,
    marginHorizontal: 20,
    flex: 1,
    bottom: -40,
    borderRadius: 18,
    elevation: 10,
    padding: 20,
    justifyContent: 'center',
  },
  comment: {
    marginTop: 10,
    color: COLORS.primary,
    lineHeight: 20,
    marginHorizontal: 20,
    top: 10,
    ...FONTS.body4
  },
  footer: {
    height: 170,
    // backgroundColor: COLORS.light,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 30,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  iconCon: {
    backgroundColor: COLORS.primary,
    width: 50,
    height: 50,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
  btn: {
    backgroundColor: COLORS.primary,
    flex: 1,
    height: 50,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    padding: 20,
    justifyContent: 'space-between',
  },
});

export default DetailsScreen;
