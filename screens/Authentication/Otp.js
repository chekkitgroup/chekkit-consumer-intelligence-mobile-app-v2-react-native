<<<<<<< HEAD
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import AuthLayout from './AuthLayout';
import OTPInputView from '@twotalltotems/react-native-otp-input';

import { FONTS, SIZES, COLORS, icons, images } from '../../constants';
import {
  TextButton,
  Header,
  IconButton
} from '../../components';

import { utils } from '../../utils';
import { SafeAreaView } from 'react-native-safe-area-context';

const Otp = ({ navigation, route }) => {

    const [timer, setTimer] = useState(60);
 
  const renderHeader = () => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white' }}>
        <Header 
           title="Verify Your Account"
           containerStyle={{
               height: 20,
               marginHorizontal: SIZES.padding,
               marginTop: 10,
               backgroundColor: COLORS.white,
               
           }}
           leftComponent={
               <IconButton 
                   icon={icons.back}
                   containerStyle={{
                       width: 40,
                       height: 22,
                       justifyContent: 'center',
                       alignItems: 'center',
                       // borderWidth: 1,
                    //    borderRadius: SIZES.radius,
                    //    borderColor: COLORS.white
                   }}
                   iconStyle={{
                       width: 20,
                       height: 20,
                       tintColor: COLORS.primary
                   }}
                   onPress={() => navigation.navigate('OtpTwo')}
               />
           }
        />
        </SafeAreaView>
    )
 }


  return (
    <>
    {renderHeader()}
    <AuthLayout>

    <View style={{ alignItems: 'center' }}>
          <Image
            source={images.outline}
            resizeMode="contain"
            style={styles.logo}
          />
    </View>

    <View style={styles.containerOTP}>
        <OTPInputView
          pinCount={4}
          style={{ height: 50, width: '100%' }}
          codeInputFieldStyle={styles.codeInputFieldStyle}
          onCodeFilled={code => {
             console.log('code: ', code);
          }}
        />
        {/* Countdown Timer */}
        <View style={styles.countdownTimer}>
          <Text style={{ color: COLORS.darkGray, ...FONTS.body3, }}>
            Didn't receive code?
          </Text>
          <TextButton
            label={`Resend (${timer}s)`}
            disabled={timer === 0 ? false : true}
            buttonContainerStyle={{
              marginLeft: SIZES.base,
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.primary,
              ...FONTS.h3,
            }}
            onPress={() => setTimer(60)}
          />
        </View>
      </View>
      {/* Footer */}
      <View style={{  }}>
        <TextButton
          label="Continue"
          buttonContainerStyle={{
            ...styles.continueBtn,
          }}
          onPress={() => navigation.navigate('OtpTwo')}
        />
        <View style={{ marginTop: SIZES.padding, alignItems: 'center' }}>
          <Text style={{ color: COLORS.darkGray, ...FONTS.body3, 
            marginBottom: Platform.OS === 'ios' ? 30 : -100 }}>
            
          </Text>
          <TextButton
            buttonContainerStyle={{
              backgroundColor: null,
              marginTop:30
            }}
            labelStyle={{
              color: COLORS.primary,
              ...FONTS.body3,
            }}
            onPress={() => console.log('TnC')}
          />
        </View>
      </View>


    </AuthLayout>
    </>
  );
};

const styles = StyleSheet.create({
  containerOTP: {
    flex: 1,
    marginTop: SIZES.padding * 2,
  },
  codeInputFieldStyle: {
    width: 65,
    height: 65,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.lightGray2,
    color: COLORS.black,
    ...FONTS.h3,
  },
  countdownTimer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: SIZES.padding,
  },
  continueBtn: {
    height: 60,
    alignItems: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
  logo: {
    height: 50,
    width: 150,
    top: -40
  },
});

export default Otp;
=======
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet,
    SafeAreaView,
    StatusBar,
    ImageBackground
} from 'react-native';
import { AuthLayout } from '../';
import { FONTS, SIZES, COLORS, icons } from "../../constants"; 
import { utils } from '../../utils';
import OTPInputView  from '@twotalltotems/react-native-otp-input';

import { FormInput } from '../../components'; 

const Otp = ({navigation}) => {
    
    const [timer, setTimer] = React.useState(60)   
    
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: COLORS.white}}>
        <StatusBar backgroundColor='#fff' 
            
        />
        
       <AuthLayout
         title="Verify Your Account"
       >
           <View style={{
               justifyContent: 'center',
               alignItems: 'center'
           }}>
                <Image 
                source={require('../../assets/images/otp.png')}
                style={{
                    height: 50,
                    marginTop: 8,                   
                    
                  }}
              />
           </View>
               


        <View
            style={{
                flex: 1,
                marginTop: SIZES.padding * 2
            }}
        >
            
            <OTPInputView 
                pinCount={4}
                style={{
                    width: "100%",
                    height: 50
                }}
                codeInputFieldStyle={{
                    width: 65,
                    height: 65,
                    borderRadius: SIZES.radius,
                    backgroundColor: COLORS.light,
                    color: COLORS.black,
                    ...FONTS.h3
                }}
                onCodeFilled={(code) => {
                    console.log(code)
                }}
            />

            <View 
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginTop: SIZES.padding
                }}
            >
               <Text
            onPress={() => navigation.navigate('SignUp')}
            style={{
              color: '#153853',              
              textAlign: 'center',
              fontSize: 16,
            }}>
            Didn't get a code <Text 
            onPress={() => navigation.navigate('Home')}
            style={{
              color: '#153853',
              fontWeight: 'bold',
              textAlign: 'center',
              fontSize: 16,
            }}>Resend SMS</Text>
          </Text>
                
            </View>
        </View>



       </AuthLayout>
       </SafeAreaView>
    )
}

export default Otp;



const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        padding: 20,
        justifyContent: 'space-between',
      },
  })


>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
