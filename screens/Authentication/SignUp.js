<<<<<<< HEAD
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Platform } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import countryList from 'country-list';

import AuthLayout from './AuthLayout';

import { FONTS, SIZES, COLORS, icons } from '../../constants';
import {
  FormInput,
  CustomSwitch,
  TextButton,
  TextIconButton,
} from '../../components';
import { utils } from '../../utils';

const countries = countryList.getData();  

const SignUp = ({ navigation, route }) => {
  const [email, setEmail] = useState('');
  const [fullname, setFullName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [phone, setPhone] = useState('');
  const [country, setCountry] = useState(countries[0].code);

  const [showPassword, setShowPassword] = useState(false);

  const [saveMe, setSaveMe] = useState(false);

  const [emailError, setEmailError] = useState('');
  const [fullnameError, setFullNameError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [phoneError, setPhoneError] = useState('');

  const isEnableSignUp = () => {
    return (
      email !== '' &&
      password !== '' &&
      confirmPassword !== '' &&
      fullname !== '' &&
      phone !== '' &&
      emailError === '' &&
      passwordError === '' &&
      fullnameError === '' &&
      phoneError === '' &&
      confirmPasswordError === '' 
    );
  };

  return (
    <AuthLayout
      title="Let's Get You Started"
    
      
      titleContainerStyle={{
        marginTop: 20,
      }}>
      {/* Form Input And Sign Up */}
      <View style={styles.container}>
        
      <View style={styles.row}>
          {/* <Text style={{
            color: COLORS.gray, ...FONTS.body4,
            top: -40
          }}> Select Country</Text> */}
          <Picker selectedValue={country} onValueChange={setCountry} style={{
            top: Platform.OS === 'ios' ? -50 : -40
          }}>
            {countries.map(country => (
              <Picker.Item value={country.code} label={country.name} />
            ))}
          </Picker>
        </View>
        <View style={{
          top: Platform.OS === 'ios' ? -80 : -50
        }}>
        {/* Form Inputs */}
        <FormInput
          label="Email"
          keyboardType="email-address"
          autoComplete="email"
          onChange={value => {
            //validate email
            utils.validateEmail(value, setEmailError);
            setEmail(value);
          }}
          errorMsg={emailError}
          appendComponent={
            <View style={styles.appendComponentEmail}>
              <Image
                source={
                  email == '' || (email != '' && emailError == '')
                    ? icons.correct
                    : icons.cancel
                }
                style={[
                  styles.imageCorrect,
                  {
                    tintColor:
                      email == ''
                        ? COLORS.gray
                        : email != '' && emailError == ''
                        ? COLORS.green
                        : COLORS.red,
                  },
                ]}
              />
            </View>
          }
        />
        <FormInput
          label="Full Name"
          keyboardType="default"
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          autoComplete="off"
          onChange={value => {
            //validate email
            // utils.validateEmail(value, setEmailError);
            setFullName(value);
          }}
          errorMsg={fullnameError}
          appendComponent={
            <View style={styles.appendComponentEmail}>
              <Image
                source={
                  fullname == '' || (fullname != '' && fullnameError == '')
                    ? icons.correct
                    : icons.cancel
                }
                style={[
                  styles.imageCorrect,
                  {
                    tintColor:
                      fullname == ''
                        ? COLORS.gray
                        : fullname != '' && fullnameError == ''
                        ? COLORS.green
                        : COLORS.red,
                  },
                ]}
              />
            </View>
          }
        />

        <FormInput
          label="Password"
          autoComplete="password"
          onChange={value => {
            utils.validatePassword(value, setPasswordError);
            setPassword(value);
          }}
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          secureTextEntry={!showPassword}
          errorMsg={passwordError}
          appendComponent={
            <TouchableOpacity
              style={styles.appendComponentPassword}
              onPress={() => setShowPassword(!showPassword)}>
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{ height: 20, width: 20, tintColor: COLORS.gray }}
              />
            </TouchableOpacity>
          }
        />

        <FormInput
          label="Confirm Password"
          autoComplete="password"
          onChange={value => {
            utils.validatePassword(value, setConfirmPasswordError);
            setConfirmPassword(value);
          }}
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          secureTextEntry={!showPassword}
          errorMsg={passwordError}
          appendComponent={
            <TouchableOpacity
              style={styles.appendComponentPassword}
              onPress={() => setShowPassword(!showPassword)}>
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{ height: 20, width: 20, tintColor: COLORS.gray }}
              />
            </TouchableOpacity>
          }
        />



        <FormInput
          label="Phone"
          keyboardType="phone-pad"
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          autoComplete="off"
          onChange={value => {
            //validate email
            // utils.validateEmail(value, setEmailError);
            setPhone(value);
          }}
          errorMsg={phoneError}
          appendComponent={
            <View style={styles.appendComponentEmail}>
              {/* <Image
                source={
                  fullname == '' || (phone != '' && phoneError == '')
                    ? icons.correct
                    : icons.cancel
                }
                style={[
                  styles.imageCorrect,
                  {
                    tintColor:
                      phone == ''
                        ? COLORS.gray
                        : fullname != '' && phoneError == ''
                        ? COLORS.green
                        : COLORS.red,
                  },
                ]}
              /> */}
            </View>
          }
        />
        {/* Save me & Forgot Password */}
        {/* <View style={styles.row}> */}
          {/* <CustomSwitch value={saveMe} onChange={value => setSaveMe(value)} /> */}
          {/* <TextButton
            label="Forgot Password?"
            buttonContainerStyle={{ backgroundColor: null }}
            labelStyle={{ color: COLORS.gray, ...FONTS.body4 }}
            onPress={() => navigation.navigate('ForgotPassword')}
          /> */}

        
        {/* </View> */}
        {/* Sign Up */}
        <TextButton
          label="Sign Up "
          
          buttonContainerStyle={{
            height: 55,
            alignItems: 'center',
            marginTop: SIZES.padding,
            borderRadius: SIZES.radius,
            backgroundColor: isEnableSignUp()
              ? COLORS.primary
              : COLORS.transparentPrimary,
          }}
          onPress={() => navigation.navigate('Otp')}
        />
       
        {/* Footer */}
         {/* Sign in */}
         <View style={styles.rowSignUp}>
          <Text
            style={{
              color: COLORS.darkGray,
              ...FONTS.body3,
            }}>
            Already have an account?
          </Text>
          <TextButton
            label=" Log In"
            buttonContainerStyle={{
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.primary,
      
            }}
            onPress={() => navigation.navigate('SignIn')}
          />
        </View>
        </View>
      </View>
    </AuthLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  appendComponentEmail: {
    justifyContent: 'center',
    // borderWidth: 1
  },
  imageCorrect: {
    height: 20,
    width: 20,
    // tintColor: COLORS.green,
  },
  appendComponentPassword: {
    // width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
  },
  row: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    justifyContent: 'space-between',
  },
  rowSignUp: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    justifyContent: 'center',
  },
  facebook: {
    height: 60,
    alignItems: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.blue,
    // marginTop: SIZES.base,
    marginTop: SIZES.base * 2,
  },
  row: {
    top: -10
  },
});

=======
import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Keyboard,
  ScrollView,
  Alert,
} from 'react-native';

// import {Picker} from '@react-native-picker/picker';


import { FONTS, COLORS } from '../../constants'
import {Loader, Input, Button} from '../../components';

const SignUp = ({navigation}) => {
  const [inputs, setInputs] = React.useState({
    email: '',
    fullname: '',
    phone: '',
    password: '',
  });
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  // const [selectedCountry, setSelectedCountry] = React.useState();

  const validate = () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!inputs.email) {
      handleError('Please input email', 'email');
      isValid = false;
    } else if (!inputs.email.match(/\S+@\S+\.\S+/)) {
      handleError('Please input a valid email', 'email');
      isValid = false;
    }

    if (!inputs.fullname) {
      handleError('Please input fullname', 'fullname');
      isValid = false;
    }

    if (!inputs.phone) {
      handleError('Please input phone number', 'phone');
      isValid = false;
    }

    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    } else if (inputs.password.length < 5) {
      handleError('Min password length of 5', 'password');
      isValid = false;
    }

    if (isValid) {
      register();
    }
  };

  const register = () => {
    setLoading(true);
    setTimeout(() => {
      try {
        setLoading(false);
        AsyncStorage.setItem('userData', JSON.stringify(inputs));
        navigation.navigate('SignIn');
      } catch (error) {
        Alert.alert('Error', 'Something went wrong');
      }
    }, 3000);
  };

  const handleOnchange = (text, input) => {
    setInputs(prevState => ({...prevState, [input]: text}));
  };
  const handleError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };
  return (
    <SafeAreaView style={{backgroundColor: COLORS.white, flex: 1}}>
      <Loader visible={loading} />
      <ScrollView
        contentContainerStyle={{paddingTop: 50, paddingHorizontal: 20}}>
        <Text style={{color: '#153853', fontSize: 25, fontWeight: 'bold'}}>
          Let's Get You Started
        </Text>
        {/* <Text style={{color: COLORS.grey, fontSize: 18, marginVertical: 10}}>
          Enter Your Details to Register
        </Text> */}
        <View style={{marginVertical: 20}}>
          
        {/* <Picker
          selectedValue={selectedCountry}
          onValueChange={(itemValue, itemIndex) =>
            setSelectedLanguage(itemValue)
          }>
          <Picker.Item label="Nigeria" value="nigeria" />
          <Picker.Item label="Afghanistan" value="afghanistan" />
        </Picker> */}
          
          
          <Input
            onChangeText={text => handleOnchange(text, 'email')}
            onFocus={() => handleError(null, 'email')}
            iconName="email-outline"
            label="Email"
            placeholder="Enter your email address"
            error={errors.email}
          />

          <Input
            onChangeText={text => handleOnchange(text, 'fullname')}
            onFocus={() => handleError(null, 'fullname')}
            iconName="account-outline"
            label="Full Name"
            placeholder="Enter your full name"
            error={errors.fullname}
          />

          <Input
            keyboardType="numeric"
            onChangeText={text => handleOnchange(text, 'phone')}
            onFocus={() => handleError(null, 'phone')}
            iconName="phone-outline"
            label="Phone Number"
            placeholder="Enter your phone no"
            error={errors.phone}
          />
          <Input
            onChangeText={text => handleOnchange(text, 'password')}
            onFocus={() => handleError(null, 'password')}
            iconName="lock-outline"
            label="Password"
            placeholder="Enter your password"
            error={errors.password}
            password
          />
          <Button title="Register" onPress={validate} />
          <Text
            onPress={() => navigation.navigate('SignIn')}
            style={{
              color: '#153853',
              
              textAlign: 'center',
              fontSize: 16,
            }}>
            Already have account? <Text
            style={{
              color: '#153853',
              fontWeight: 'bold',
              textAlign: 'center',
              fontSize: 16,
            }}
            >Login In</Text>
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
export default SignUp;
