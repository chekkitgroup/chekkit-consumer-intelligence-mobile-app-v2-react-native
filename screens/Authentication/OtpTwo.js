import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, StatusBar } from 'react-native';
import AuthLayout from './AuthLayout';
import OTPInputView from '@twotalltotems/react-native-otp-input';

import { FONTS, SIZES, COLORS, icons, images } from '../../constants';
import {
  TextButton,
  Header,
  IconButton
} from '../../components';

import { utils } from '../../utils';
import { SafeAreaView } from 'react-native-safe-area-context';

const OtpTwo = ({ navigation, route }) => {

    const [timer, setTimer] = useState(60);
 
  const renderHeader = () => {
    return (

        <SafeAreaView 
          style={{ 
            backgroundColor: '#D0D7DD',
            height: 380,
            width: "100%"
          
          }}
        >
        <Header 
           title="Verify Your Account"
           containerStyle={{
               height: 20,
               marginHorizontal: SIZES.padding,
               marginTop: 10,
               backgroundColor: '#D0D7DD',
               
           }}
           leftComponent={
               <IconButton 
                   icon={icons.back}
                   containerStyle={{
                       width: 40,
                       height: 22,
                       justifyContent: 'center',
                       alignItems: 'center',
                       // borderWidth: 1,
                    //    borderRadius: SIZES.radius,
                    //    borderColor: COLORS.white
                   }}
                   iconStyle={{
                       width: 20,
                       height: 20,
                       tintColor: COLORS.primary
                   }}
                   onPress={() => navigation.navigate('Otp')}
               />
           }
        />

<View style={{ alignItems: 'center' }}>
          <Image
            source={images.outline}
            resizeMode="contain"
            style={styles.logo}
          />
        </View>
    
    <View style={styles.containerOTP2}>
    <OTPInputView
      pinCount={4}
      style={{ height: 50, width: '100%' }}
      codeInputFieldStyle={styles.codeInputFieldStyle}
      onCodeFilled={code => {
         console.log('code: ', code);
      }}
    />
    {/* Countdown Timer */}
    <View style={styles.countdownTimer}>
      <Text style={{ color: COLORS.darkGray, ...FONTS.body3 }}>
        Didn't receive code?
      </Text>
      <TextButton
        label={`Resend (${timer}s)`}
        disabled={timer === 0 ? false : true}
        buttonContainerStyle={{
          marginLeft: SIZES.base,
          backgroundColor: null,
        }}
        labelStyle={{
          color: COLORS.primary,
          ...FONTS.h3,
        }}
        onPress={() => setTimer(60)}
      />
    </View>
  </View>

   
        </SafeAreaView>
    )
 }


  return (
    <>
    {renderHeader()}

    <AuthLayout>

  {/* Footer */}
  <View style={{ marginBottom: 250 }}>
      <View style={{ alignItems: 'center' }}>
          <Image
            source={images.icon_like}
            resizeMode="contain"
            style={styles.iconlike}
            
          />
        </View>
        <Text style={{ color: COLORS.primary, ...FONTS.body2, textAlign: 'center' }}>
        Account Verified
      </Text>
      <Text style={{ marginBottom: Platform.OS === 'ios' ? 30 : 40, color: COLORS.primary, ...FONTS.body4, textAlign: 'center' }}>
        You have successfully verified your account
      </Text>
    <TextButton
      label="Done"
      buttonContainerStyle={{
        ...styles.continueBtn,
      }}
      onPress={() => navigation.navigate('MainApp')}
    />
    {/* <View style={{ marginTop: SIZES.padding, alignItems: 'center' }}>
      <Text style={{ color: COLORS.darkGray, ...FONTS.body3 }}>
        By signing up, you agree to our.
      </Text>
      
    </View> */}
  </View>


  </AuthLayout>

    </>
  );
};

const styles = StyleSheet.create({
  containerOTP: {
    flex: 1,
    marginTop: SIZES.padding * 2,
  },
  containerOTP2: {
    flex: 1,
    paddingTop: SIZES.padding,
    marginRight: 15,
    marginLeft: 15,
    
    
    marginTop: SIZES.padding * 3,
  },
  codeInputFieldStyle: {
    width: 65,
    height: 65,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.lightGray2,
    color: COLORS.black,
    ...FONTS.h3,
  },
  countdownTimer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: SIZES.padding,
  },
  continueBtn: {
    height: 60,
    alignItems: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
  },
  logo: {
    height: 50,
    width: 150,
    top: 40
  },
  iconlike: {
    height: 50,
    width: 250,
    bottom: Platform.OS === 'ios' ? 20 : 70
  }
});

export default OtpTwo;