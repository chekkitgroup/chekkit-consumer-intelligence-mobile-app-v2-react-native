import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import AuthLayout from './AuthLayout';


import { FONTS, SIZES, COLORS, icons, images } from '../../constants';
import {
  FormInput,
  CustomSwitch,
  TextButton,
  Header,
  IconButton,
  TextIconButton,
  Alert,
  Keyboard
} from '../../components';
import { utils } from '../../utils';
import { SafeAreaView } from 'react-native-safe-area-context';

const PasswordSetting = ({ navigation, route }) => {

  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [saveMe, setSaveMe] = useState(false);

  const isEnableSignIn = () => {
    return email !== '' && password !== '' && emailError === '';
  };

  const renderHeader = () => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white' }}>
        <Header 
           title="Password Setting"
           containerStyle={{
               height: 20,
               marginHorizontal: SIZES.padding,
               marginTop: 10,
               backgroundColor: COLORS.white,
               
           }}
           leftComponent={
               <IconButton 
                   icon={icons.back}
                   containerStyle={{
                       width: 40,
                       height: 22,
                       justifyContent: 'center',
                       alignItems: 'center',
                       // borderWidth: 1,
                    //    borderRadius: SIZES.radius,
                    //    borderColor: COLORS.white
                   }}
                   iconStyle={{
                       width: 20,
                       height: 20,
                       tintColor: COLORS.black
                   }}
                   onPress={() => navigation.navigate('SignIn')}
               />
           }
        />
        </SafeAreaView>
    )
 }


  return (
    <>
    {renderHeader()}
    <AuthLayout>
      <View style={styles.container}>
        {/* Form Inputs */}
        <FormInput
          label="Old Password"
          autoComplete="password"
          onChange={value => {
            setPassword(value);
          }}
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          secureTextEntry={!showPassword}
          appendComponent={
            <TouchableOpacity
              style={styles.appendComponentPassword}
              onPress={() => setShowPassword(!showPassword)}>
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{ height: 20, width: 20, tintColor: COLORS.gray }}
              />
            </TouchableOpacity>
          }
        />

        <FormInput
          label="new Password"
          autoComplete="password"
          onChange={value => {
            setPassword(value);
          }}
          containerStyle={{
            marginTop: SIZES.radius,
          }}
          secureTextEntry={!showPassword}
          appendComponent={
            <TouchableOpacity
              style={styles.appendComponentPassword}
              onPress={() => setShowPassword(!showPassword)}>
              <Image
                source={showPassword ? icons.eye_close : icons.eye}
                style={{ height: 20, width: 20, tintColor: COLORS.gray }}
              />
            </TouchableOpacity>
          }
        />
        {/* Save me & Forgot Password */}
        {/* <View style={styles.row}>
          <CustomSwitch value={saveMe} onChange={value => setSaveMe(value)} />
          <TextButton
            label="Trouble loggin in?"
            buttonContainerStyle={{ backgroundColor: null }}
            labelStyle={{ color: COLORS.gray, ...FONTS.body4 }}
            onPress={() => navigation.navigate('ForgotPassword')}
          />
        </View> */}
        {/* Sign In */}
        <TextButton
          label="Change Password"
         
          buttonContainerStyle={{
            height: 55,
            alignItems: 'center',
            marginTop: SIZES.padding,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.primary
             
          }}
          onPress={() => navigation.navigate('MainApp')}
        />
        {/* Sign Up */}
        {/* <View style={styles.rowSignUp}>
          <Text
            style={{
              color: COLORS.darkGray,
              ...FONTS.body3,
            }}>
            Don't have an account?
          </Text>
          <TextButton
            label=" Sign Up"
            buttonContainerStyle={{
              backgroundColor: null,
            }}
            labelStyle={{
              color: COLORS.primary,
              ...FONTS.h3,
            }}
            onPress={() => navigation.navigate('SignUp')}
          />
        </View> */}
        
         
           
      </View>
    </AuthLayout>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: SIZES.padding * 2,
  },
  appendComponentEmail: {
    justifyContent: 'center',
    // borderWidth: 1
  },
  imageCorrect: {
    height: 20,
    width: 20,
    // tintColor: COLORS.green,
  },
  appendComponentPassword: {
    // width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
  },
  row: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    justifyContent: 'space-between',
  },
  rowSignUp: {
    flexDirection: 'row',
    marginTop: SIZES.radius,
    justifyContent: 'center',
  },
  facebook: {
    height: 50,
    alignItems: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.blue,
    marginTop: SIZES.base,
  },
});

export default PasswordSetting;
