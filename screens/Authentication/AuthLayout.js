<<<<<<< HEAD
import React from 'react';
import {
  View,
  Text,
  ViewStyle,
  ViewComponent,
  StyleSheet,
  Image,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { images, FONTS, SIZES, COLORS } from '../../constants';

const AuthLayout = ({
  title,
  subtitle,
  titleContainerStyle,
  children,
}) => {
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        scrollEnabled={true}
        style={{
          flex: 1,
          backgroundColor: COLORS.white,
          // borderTopColor: colors.SHADOW,
          // borderTopWidth: 1,
        }}
        enableAutomaticScroll={true}
        keyboardDismissMode="on-drag"
        enableOnAndroid={true}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flex: 1,
          paddingHorizontal: SIZES.padding,
        }}>
        {/* App Icon */}
        <View style={{ alignItems: 'center' }}>
          {/* <Image
            source={images.logo_02}
            resizeMode="contain"
            style={styles.logo}
          /> */}
        </View>
        {/* Title & Subtitle */}
        <View style={{ marginTop: SIZES.padding, ...titleContainerStyle, }}>
          <Text style={{ textAlign: 'left', ...FONTS.h3, color: '#153853' }}>
            {title}
          </Text>
          <Text
            style={{
              textAlign: 'center',
              color: COLORS.darkGray,
              marginTop: SIZES.base,
              ...FONTS.body3,
            }}>
            {subtitle}
          </Text>
        </View>
        {/* Content / Children */}
        {children}
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingVertical: SIZES.padding,
    paddingTop: SIZES.padding,
    backgroundColor: COLORS.white,
    // backgroundColor: 'red',
  },
  logo: {
    height: 100,
    width: 200,
  },
});

export default AuthLayout;
=======
import React from 'react'
import { 
    StyleSheet, 
    Text, 
    View 
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {images, FONTS, SIZES, COLORS } from "../../constants"


const  AuthLayout = ({title, subtitle, titleContainerStyle, children}) => {
    return (
        <View style={{
            flex: 1,
            paddingVertical: SIZES.padding,
            backgroundColor: COLORS.white,
        }}>
            <KeyboardAwareScrollView
                keyboardDismissMode='on-drag'
                contentContainerStyle={{
                    flex: 1,
                    paddingHorizontal: SIZES.padding
                }}
            >

            <View
                style={{
                    marginTop: SIZES.padding,
                    ...titleContainerStyle
                }}
            >
                <Text
                    style={{
                        textAlign: 'left',
                        ...FONTS.h2,
                        color: COLORS.primary,
                        fontWeight: 'bold'
                    }}
                >
                    {title}
                    
                </Text>
                <Text
                    style={{
                        textAlign: 'center',
                        color: COLORS.darkGray2,
                        marginTop: 20,
                        ...FONTS.body3,
                        textAlign: 'left'
                    }}
                >
                    {subtitle}
                </Text>
            </View>
                {children}
            </KeyboardAwareScrollView>
        </View>
    )
}






export default AuthLayout;


const styles = StyleSheet.create({})
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
