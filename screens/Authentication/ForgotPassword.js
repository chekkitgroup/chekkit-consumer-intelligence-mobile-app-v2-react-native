<<<<<<< HEAD
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AuthLayout from './AuthLayout';

import { FONTS, SIZES, COLORS, icons } from '../../constants';
import {
  FormInput,
  CustomSwitch,
  TextButton,
  TextIconButton,
} from '../../components';
import { utils } from '../../utils';

const ForgotPassword = ({
  navigation,
  route,
}) => {
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');

//   const insets = useSafeAreaInsets();

  const isEnableSendEmail = () => {
    return email !== '' && emailError === '';
  };

  return (
    <AuthLayout
      title="Password Recovery"
      subtitle="Enter your email and we will send you a link to reset your password"
      titleContainerStyle={{
        marginTop: SIZES.padding * 2,
      }}>
      {/* Form Input */}
      <View style={styles.container}>
        <FormInput
          label="Email"
          keyboardType="email-address"
          autoComplete="email"
          onChange={value => {
            //validate email
            utils.validateEmail(value, setEmailError);
            setEmail(value);
          }}
          errorMsg={emailError}
          appendComponent={
            <View style={{ justifyContent: 'center' }}>
              <Image
                source={
                  email == '' || (email != '' && emailError == '')
                    ? icons.correct
                    : icons.cancel
                }
                style={[
                  styles.imageCorrect,
                  {
                    tintColor:
                      email == ''
                        ? COLORS.gray
                        : email != '' && emailError == ''
                        ? COLORS.green
                        : COLORS.red,
                  },
                ]}
              />
            </View>
          }
        />
      </View>

      {/* Button */}
      {/* <TextButton
        label="Send Email"
        buttonContainerStyle={{
          ...styles.continueBtn,
          marginBottom: insets.bottom,
          backgroundColor: isEnableSendEmail()
            ? COLORS.primary
            : COLORS.transparentPrimary,
        }}
        disabled={isEnableSendEmail() ? false : true}
        onPress={() => navigation.goBack()}
      /> */}
    <View style={{
        top: 50
    }}>
        <TextButton
          label="Send Email"
          disabled={isEnableSendEmail() ? false : true}
          buttonContainerStyle={{
            height: 55,
            alignItems: 'center',
            marginBottom: 400,
            marginTop: SIZES.padding,
            borderRadius: SIZES.radius,
            backgroundColor: isEnableSendEmail()
              ? COLORS.primary
              : COLORS.transparentPrimary,
          }}
          onPress={() => navigation.goBack()}
        />
    </View>
    </AuthLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: SIZES.padding * 2,
  },
  imageCorrect: {
    height: 20,
    width: 20,
  },
  continueBtn: {
    height: 60,
    alignItems: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary,
    marginTop: SIZES.padding,
  },
});

export default ForgotPassword;
=======
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native';
import { AuthLayout } from '../';
import { FONTS, SIZES, COLORS, icons } from "../../constants"; 
import { utils } from '../../utils';
import { FormInput } from '../../components';

const ForgotPassword = ({navigation}) => {
    
    const [email, setEmail] = React.useState("")
    const [emailError, setEmailError] = React.useState("")
    
    return (
        <AuthLayout
            title="Forgot Your Password"
            subtitle="Enter your email and we will send you a link to reset your password"
            titleContainerStyle={{
                marginTop: SIZES.padding * 2
            }}

        >
         <View
            style={{
                flex: 1,
                marginTop: SIZES.padding * 2
            }}
         >
             
             <FormInput 
            label='Email'
            keyboardType='email-address'
            autoCompleteType='email'
            onChange={(value) => {
              utils.validateEmail(value, setEmailError)

              setEmail(value)
            }}
            errorMsg={emailError}
            appendComponent={
              <View
                style={{
                  justifyContent: 'center'
                }}
              >
                <Image source={email == "" || (email != "" && emailError == "") ?  icons.correct : icons.cancel } 
                  style={{
                    height: 20,
                    width: 20,
                    tintColor: email == "" ? COLORS.gray : (email != "" && emailError == "") ? COLORS.green : COLORS.red
                  }}
                />
              </View>
            }
          />
          
            <View style={styles.btn}>
              <Text style={{color: COLORS.white, fontSize: 17,}}>
                Reset Password
              </Text>
            </View>
           
            
        </View>   
        
        </AuthLayout>
    )
}

export default ForgotPassword;

const styles = StyleSheet.create({
    btn: {
      height: 55,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 30,
      backgroundColor: COLORS.primary,
      marginHorizontal: 3,
      borderRadius: 10,
    },
  })
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
