import React from 'react';
import {
    Animated,
    Image,
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';

// constants
import { images, theme } from "../../constants";
const { onboarding1, onboarding2, onboarding3 } = images;

// theme
const { COLORS, FONTS, SIZES } = theme;

const onBoardings = [
    {
        title: "Protect yourself with Chekkit App by verifying the safety of products before you use them!",
        img: onboarding1
    },
    {
        title: "Scan Products & Win Amazing Rewards",
        img: onboarding2
    },
    {
        title: "Protect yourself with Chekkit App by verifying the safety of products before you use them!",
        img: onboarding3
    }
];

const OnBoarding = ({navigation}) => {
    const [completed, setCompleted] = React.useState(false);

    const scrollX = new Animated.Value(0);

    React.useEffect(() => {
        scrollX.addListener(({ value }) => {
            if (Math.floor(value / SIZES.width) === onBoardings.length - 1) {
                setCompleted(true);
            }
        });

        return () => scrollX.removeListener();
    }, []);

    // Render

    function renderContent() {
        return (
            <Animated.ScrollView
                horizontal
                pagingEnabled
                scrollEnabled
                decelerationRate={0}
                scrollEventThrottle={16}
                snapToAlignment="center"
                showsHorizontalScrollIndicator={false}
                onScroll={Animated.event([
                    { nativeEvent: { contentOffset: { x: scrollX } } },
                ], { useNativeDriver: false })}
            >
                {onBoardings.map((item, index) => (
                    <View
                        //center
                        //bottom
                        key={`img-${index}`}
                        style={styles.imageAndTextContainer}
                    >
                        <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                source={item.img}
                                resizeMode="cover"
                                style={{
                                    width: "100%",
                                    height: "100%",
                                }}
                            />
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                bottom: '38%',
                                left: 40,
                                right: 40
                            }}
                        >
                            <Text style={{
                                ...FONTS.h1,
                                color: COLORS.gray,
                                textAlign: 'center',
                            }}
                            >
                                {item.title}
                            </Text>

                            <View>
<<<<<<< HEAD
                            <TouchableOpacity>
=======
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
                                <Text style={{
                                    ...FONTS.body3,
                                    textAlign: 'center',
                                    marginTop: SIZES.base,
                                    color: '#46F7AD',
                                    top: 80,
                                    textDecorationLine: 'underline'
                                }}
<<<<<<< HEAD
                                onPress={() => navigation.navigate("Scan")}
                                > Skip and proceed to scan</Text>
                                </TouchableOpacity>
=======
                                >
                                    Skip and proceed to scan
                                </Text>
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
                            </View>

                            
                        </View>
                        <View
                            style={{
                                position: 'absolute',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                bottom: 35
                            }}
                        >
                            
                            <TouchableOpacity
                            style={{
                                flex: 1,
                                height: 70,
                                marginRight: 15,
                                marginLeft: 15,
                                backgroundColor: 'transparent',
                                borderWidth: 2,
                                borderColor: COLORS.white,
                                alignItems: 'center',
                                justifyContent: 'center',
                                borderRadius: 10,
                            }}
<<<<<<< HEAD
                            onPress={() => navigation.navigate("SignUp")}
=======
                            onPress={() => navigation.navigate("SignIn")}
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
                        >
                            <Text style={{ ...FONTS.h3, color: COLORS.white }}>Get Started</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{
                                flex: 1,
                                height: 70,
                                backgroundColor: '#46F7AD',
                                alignItems: 'center',
                                justifyContent: 'center',
                                borderRadius: 10,
                                marginLeft: 15,
                                marginRight: 15,
                            }}
                            onPress={() => navigation.navigate("SignIn")}
                        >
                            <Text style={{ ...FONTS.h3, color: '#153853' }}>Log In</Text>
                        </TouchableOpacity>
                        </View>


                        {/* Button */}
                        {/* <TouchableOpacity
                            style={{
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                                width: 150,
                                height: 60,
                                paddingLeft: 20,
                                justifyContent: 'center',
                                borderTopLeftRadius: 30,
                                borderBottomLeftRadius: 30,
                                borderBottomRightRadius: 0,
                                borderTopRightRadius: 0,
                                backgroundColor: COLORS.blue
                            }}
                            onPress={() => { console.log("Button on pressed") }}
                        >
                            <Text style={{ ...FONTS.h1, color: COLORS.white }}>{completed ? "Let's Go" : "Skip"}</Text>
                        </TouchableOpacity> */}
                    </View>
                ))}
            </Animated.ScrollView>
        );
    }

    function renderDots() {

        const dotPosition = Animated.divide(scrollX, SIZES.width);

        return (
            <View style={styles.dotsContainer}>
                {onBoardings.map((item, index) => {
                    const opacity = dotPosition.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0.3, 1, 0.3],
                        extrapolate: "clamp"
                    });

                    const dotSize = dotPosition.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [SIZES.base, 17, SIZES.base],
                        extrapolate: "clamp"
                    });

                    return (
                        <Animated.View
                            key={`dot-${index}`}
                            opacity={opacity}
                            style={[styles.dot, { width: dotSize, height: dotSize, }]}
                        />
                    );
                })}
            </View>
        );
    }

    return (
        <SafeAreaView style={styles.container}>
            <View>
                {renderContent()}
            </View>
            <View style={styles.dotsRootContainer}>
                {renderDots()}
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.white
    },
    imageAndTextContainer: {
        width: SIZES.width
    },
    dotsRootContainer: {
        position: 'absolute',
        bottom: SIZES.height > 700 ? '20%' : '16%',
    },
    dotsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: SIZES.padding / 2,
        marginBottom: SIZES.padding * 20,
        height: SIZES.padding,
    },
    dot: {
        borderRadius: SIZES.radius,
        backgroundColor: '#46F7AD',
        marginHorizontal: SIZES.radius / 2
    }
});

export default OnBoarding;
