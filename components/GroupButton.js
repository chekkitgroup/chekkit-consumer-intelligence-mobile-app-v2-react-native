import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

import { FONTS, SIZES, COLORS, icons, images } from '../constants';

const GroupButton = ({activeColor, buttons, ...rest}) => {
  return (
    <View style={styles.container}> 

        {buttons.map((button) => {
           return ( <TouchableOpacity 
                        onPress={button.onPress}
                        style={{
                            flex: 1,
                            height: 48,
                            backgroundColor: button.activated ? activeColor : COLORS.white,
                            borderRadius: SIZES.radius,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}    
                    >
                <Text 
                    style={[{color: button.activated ? COLORS.white : COLORS.primary },  styles.text]}
                >
                    {button.text}
                </Text>
            </TouchableOpacity>
            )
        })}
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        margginBottom: 10,
        marginLeft: 20,
        marginRight: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        backgroundColor: 'transparent'

    },
    text: {
        
        lineHeight: 18,
        fontSize: 16,
    }
}) 

export default GroupButton