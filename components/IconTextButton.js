import React from 'react';
import {
  ViewStyle,
  TouchableOpacity,
  Image,
  Text,
  ImageStyle,
  ImageSourcePropType,
} from 'react-native';
import { color } from 'react-native-reanimated';
import { COLORS, FONTS, SIZES } from '../constants';

const IconTextButton = ({
  containerStyle,
  icon,
  iconStyle,
  onPress,
  label
}) => {
  return (
    <TouchableOpacity style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: SIZES.radius,
        backgroundColor: COLORS.primary,
        ...containerStyle
      }} 
      onPress={onPress}>
      <Image
        source={icon}
        resizeMode="contain"
        style={{ 
            width: 20, 
            height: 20 
        }}
      />
      <Text
        style={{
            marginLeft: SIZES.base,
            ...FONTS.h3,
            color: COLORS.white
        }}
      >
          {label}
      </Text>
    </TouchableOpacity>
  );
};

export default IconTextButton;
