import React from 'react';
import { View, Text, ViewStyle, StyleSheet, TextStyle } from 'react-native';
import { FONTS } from '../constants';

const Header = ({
  title,
  containerStyle,
  leftComponent,
  rightComponent,
  titleStyle,
}) => {
  return (
    <View 
      style={{ 
        height: 60,
        flexDirection: 'row',
        ...containerStyle
        }}>
      {/* Left */}
      {leftComponent}
      {/* Title */}
      <View style={styles.containerTitle}>
        <Text style={[{ ...FONTS.h3, color: '#153853', left: -60 }, titleStyle]}>
          {title}
        </Text>
      </View>
      {/* Right */}
      {rightComponent}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  containerTitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
  },
});
