<<<<<<< HEAD
import * as React from 'react';
import { LogBox } from 'react-native';
import Router from './navigation';



LogBox.ignoreLogs([
    "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
  ]);

const App = () => {
    return (
         <Router />
    );
};

export default App; 
=======
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-native-paper';
import Tabs from "./navigation/tabs";
import SplashScreen from 'react-native-splash-screen';
import { OnBoarding, SignIn, SignUp, Home, Otp } from './screens';
import { theme } from './core/theme';
const Stack = createNativeStackNavigator();

const App = () => {

    React.useEffect(() => {
        SplashScreen.hide()
    }, [])


    return (
        <Provider theme={theme}>
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false
                    }}
                    initialRouteName={'OnBoarding'}
                >
                    <Stack.Screen
                        name="MainLayout"
                        component={Tabs}
                    />

                    <Stack.Screen
                        name="OnBoarding"
                        component={OnBoarding}
                    />
                    <Stack.Screen
                        name="SignIn"
                        component={SignIn}
                    />
                    <Stack.Screen
                        name="SignUp"
                        component={SignUp}
                    />
                    <Stack.Screen
                        name="Home"
                        component={Home}
                    />
                     <Stack.Screen
                        name="Otp"
                        component={Otp}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    )
}

export default App;
>>>>>>> 41b4e715ed28cc59c93ec9040243bb4a24b19592
