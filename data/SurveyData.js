export default data = [
    {
        question: "What year was Indomie founded and how many flavours have we",
        options: ["1980 and 5","2016 and 3","2017 and 7","2018 and 4"],
        correct_option: ["1980 and 5","2016 and 3","2017 and 7","2018 and 4"],
    },
    {
        question: "How often do you eat Indomie",
        options: ["once a week","twice a week","everyday", "once a month"],
        correct_option: ["once a week","twice a week","everyday", "once a month"],
    },
    {
        question: "Which flavour of Indomie do you prefeer",
        options: ["Onion","Chicken","Pepper Soup","All"],
        correct_option: ["Onion","Chicken","Pepper Soup","All"],
    },
    {
        question: "How long does it take to make Indomie",
        options: ["5 Min","2 Min","4 Min","7 Min"],
        correct_option: ["5 Min","2 Min","4 Min","7 Min"],
    },
    {
        question: "Indomie is delicious",
        options: ["Yes","No","Always"],
        correct_option: ["Yes","No","Always"],
    }
]