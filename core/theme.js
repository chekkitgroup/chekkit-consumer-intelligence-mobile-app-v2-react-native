import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#000000',
    primary: "#46F7AD",
    green: '#46F7AD',
    tint: '#FCFCFD',
    secondary: '#414757',
    border: '#C9DDEC',
    error: '#f13a59',
    success: '#00B386',
    google: '#2E7D32',
    white: "#fff",
    green: "#46F7AD",
    red: "#D84035",
    black: "#000000",
    gray: "#CACACA",
    gold: "#F7B046",
    lightGray: "#3B3B3B",
    lightGray2: '#212125',
    lightGray3: '#757575',
  },
}
