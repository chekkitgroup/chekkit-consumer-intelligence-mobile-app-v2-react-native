const plants = [
    {
      id: 1,
      name: 'Succulent Plant',
      price: 'Nivea',
      like: true,
      img: require('../assets/images/nivea1.png'),
      about:
        'Succulent Plantis one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
  
    {
      id: 2,
      name: 'Dragon Plant',
      price: 'Indomie',
      like: false,
      img: require('../assets/images/indomie1.png'),
      about:
        'Dragon Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
    {
      id: 3,
      name: 'Ravenea Plant',
      price: 'Indomie',
      like: false,
      img: require('../assets/images/indomie1.png'),
      about:
        'Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
  
    {
      id: 4,
      name: 'Potted Plant',
      price: 'Nivea',
      like: true,
      img: require('../assets/images/nivea1.png'),
      about:
        'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
    {
      id: 5,
      name: 'Ravenea Plant',
      price: 'Indomie',
      like: true,
      img: require('../assets/images/indomie1.png'),
      about:
        'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
    {
      id: 6,
      name: 'Dragon Plant',
      price: 'Nivea',
      like: false,
      img: require('../assets/images/nivea1.png'),
      about:
        'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
    },
  ];
  
  export default plants;
  