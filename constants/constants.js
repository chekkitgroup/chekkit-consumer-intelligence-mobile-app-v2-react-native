const redeemTabs = [
    {
        id: 1,
        title: "Redeem History",
    },
    {
        id: 2,
        title: "Redeem",
    },
]

const coins = [
    {
      id: 1,
      image: require('../assets/images/bag.png'),
      description: 'Withdraw Cash',
      amount: '$500'
      },
    {
        id: 2,
        image: require('../assets/images/air.png'),
        description: 'Airtime Top-Up',
        amount: '$500'},
    {
        id: 3,
        image: require('../assets/images/bag.png'),
        description: 'Withdraw Cash',
        amount: '$500'
    },
  ];

//API
// My Holdings
//https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&order=${orderBy}&per_page=${perPage}&page=${page}&sparkline=${sparkline}&price_change_percentage=${priceChangePerc}&ids=${ids}

// Coin Market
//https://api.coingecko.com/api/v3/coins/markets?vs_currency=${currency}&order=${orderBy}&per_page=${perPage}&page=${page}&sparkline=${sparkline}&price_change_percentage=${priceChangePerc}

const constants = {
    redeemTabs,
    coins
};

export default constants;