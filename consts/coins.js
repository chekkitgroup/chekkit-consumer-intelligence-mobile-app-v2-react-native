const coins = [
    {
      id: 1,
      image: require('../assets/images/bag.png'),
      description: 'Withdraw Cash',
      description2: 'Amount',
      type: 'Points',
      amount: '500',
      points: '120'
      },
    {
        id: 2,
        image: require('../assets/images/air.png'),
        description: 'Airtime Top-Up',
        description2: 'Amount',
        type: 'Points',
        amount: '500',
        points: '250',
      },
    {
        id: 3,
        image: require('../assets/images/bag.png'),
        description: 'Withdraw Cash',
        description2: 'Amount',
        type: 'Points',
        amount: '500',
        points: '200'
    },
  ];

  export default coins;