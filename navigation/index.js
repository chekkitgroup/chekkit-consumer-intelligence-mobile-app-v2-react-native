

import { View, Text } from 'react-native'
import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RootStack from './RootStack';

const Stack = createStackNavigator();

const Router = () => {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen component={RootStack} name="Mother" />
        </Stack.Navigator>
      </NavigationContainer>
    );
  };
  
  export default Router;