import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainStack from './MainStack';
import { Survey } from '../screens';


const Stack = createStackNavigator();

const RootStack = () => {
    return (
        <Stack.Navigator
      screenOptions={{
          headerShown: false
        }}
      >
            
            <Stack.Screen 
                component={MainStack} 
                name="MyMain"
            />  

            <Stack.Screen 
                component={Survey}
                name="Survey"
                options={{title: 'Survey'}}
            />
        </Stack.Navigator>
    );
};

export default RootStack;