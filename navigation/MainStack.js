import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

import { COLORS, FONTS, SIZES, icons, images } from "../constants";

import {
  SignIn, 
  SignUp, 
  Otp, 
  ForgotPassword, 
  OnBoarding, 
  Redeem, 
  Activity, 
  Dashboard, 
  Report, 
  Details, 
  Scan,
  BrandSurvey,
  PasswordSetting,
  OtpTwo,
  Congratulations,
  RedeemPrize
} from '../screens';

import { TabIcon } from "../components";

const MainApp = () => {
    return (
      <Tab.Navigator
              screenOptions={{
                  tabBarShowLabel: false,
                  headerShown: false,
                  tabBarStyle: {
                      height: 80,
                      backgroundColor: COLORS.white,
                      borderTopColor: "transparent",
                  }
              }}
          >
        <Tab.Screen
                name="Dashboard"
                component={Dashboard} 
                options={{
                    headerShown: false,
                    tabBarIcon: ({focused}) => (
                      <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        <Image 
                          source={icons.home}
                          resizeMode="contain"
                          style={{
                            width: 20,
                            height: 20,
                            tintColor: focused ? COLORS.primary : COLORS.black
                          }}
                        />
                        <Text style={{color: focused ? COLORS.primary : COLORS.black, ...FONTS.body5}}>Home</Text>
                      </View>
                    )
                }}
            />
  
          <Tab.Screen
                name="Redeem"
                component={Redeem} 
                options={{
                  headerShown: false,
                  tabBarIcon: ({focused}) => (
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                      <Image 
                        source={icons.redeem}
                        resizeMode="contain"
                        style={{
                          width: 20,
                          height: 20,
                          tintColor: focused ? COLORS.primary : COLORS.black
                        }}
                      />
                      <Text style={{color: focused ? COLORS.primary : COLORS.black, ...FONTS.body5}}>Redeem</Text>
                    </View>
                  )
              }}
          />

       <Tab.Screen
                name="Report"
                component={Report} 
                options={{
                  headerShown: false,
                  tabBarIcon: ({focused}) => (
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                      <Image 
                        source={icons.report}
                        resizeMode="contain"
                        style={{
                          width: 20,
                          height: 20,
                          tintColor: focused ? COLORS.primary : COLORS.black
                        }}
                      />
                      <Text style={{color: focused ? COLORS.primary : COLORS.black, ...FONTS.body5}}>Report Product</Text>
                    </View>
                  )
              }}
          />
            <Tab.Screen
                name="Activity"
                component={Activity} 
                options={{
                  headerShown: false,
                  tabBarIcon: ({focused}) => (
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                      <Image 
                        source={icons.activity}
                        resizeMode="contain"
                        style={{
                          width: 20,
                          height: 20,
                          tintColor: focused ? COLORS.primary : COLORS.black
                        }}
                      />
                      <Text style={{color: focused ? COLORS.primary : COLORS.black, ...FONTS.body5}}>Activity</Text>
                    </View>
                  )
              }}
          />
      </Tab.Navigator>
    );
  }

const MainStack = () => {
    return (
      <Stack.Navigator
      screenOptions={{
          headerShown: false
        }}
      >
        
        <Stack.Screen
          name="OnBoarding"
          component={OnBoarding}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MainApp"
          component={MainApp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Details"
          component={Details}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Scan"
          component={Scan}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="BrandSurvey"
          component={BrandSurvey}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PasswordSetting"
          component={PasswordSetting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OtpTwo"
          component={OtpTwo}
          options={{headerShown: false}}
        />
         
        <Stack.Screen
          name="Congratulations"
          component={Congratulations}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Redeem"
          component={Redeem}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="RedeemPrize"
          component={RedeemPrize}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  };
  
  export default MainStack;
  
  const styles = StyleSheet.create({});